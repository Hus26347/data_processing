import random
from sklearn.utils import shuffle
import itertools
import audioSegmentation
import audioBasicIO
import pydub
import librosa
import soundfile
import pandas as pd
from pydub import AudioSegment
from pydub.playback import play
import librosa.display
from librosa import load
import numpy as np
import os
import tarfile
import sox
from  pysndfx import AudioEffectsChain
from pyAudioAnalysis import audioSegmentation as aS
from pydub import AudioSegment
from scipy.io import wavfile
from scipy.signal import butter, lfilter
import argparse
import time
import scipy.ndimage
from sound_utilities import *


"""
Creates data set from 3 files.
taken randomly.2 for voice data files, 1 noise record.
The voice files undergo  modifications: dynamic range compression, lowpass filtering, and are broken to segments of up to 10 sec duration.
Interfering noise audio is also broken to segments of 3 sec max. There is an option to apply sound effects on any segment.
The output file is created in the form of dialog ,speakerA - pause(noise)-speakerB with randomality applied to the order.
2 output files:1. Is a .wav file of max 60 sec duration, 2.wav file, similar to  previous file, but without  pauses(noise).  
Informative documentation is created for each file.
"""

# modify the paths by demand/structure of files
def get_two_files_randomly(path):
    folders_list=os.listdir(path)
    first_indx,second_indx=0,0
 #   while first_indx==second_indx:
 #   first_indx,second_indx=random.randint(0,len(folders_list)-1),random.randint(0,len(folders_list)-1)
 #   first_folder,second_folder=folders_list[first_indx],folders_list[second_indx]
    path_1=path#os.path.join(path,first_folder)
    path_2=path#os.path.join(path,second_folder)     
    files_list_1=os.listdir(path_1)
    files_list_2=os.listdir(path_2)
    index_1,index_2='',''
    while index_1==index_2:  
        index_1,index_2=random.randint(0,len(files_list_1)-1),random.randint(0,len(files_list_2)-1)
    return path_1,files_list_1[index_1],path_2,files_list_2[index_2]


def get_file_randomly(path):
    files_list_1=os.listdir(path)
    index_1=random.randint(0,len(files_list_1)-1)
    return path,files_list_1[index_1]

    
def clean_silence(audio,segments,fs):    
    merged_segments=[]
    for seg in segments:
        start=int(seg[0]*fs)
        end=int(seg[1]*fs)
        merged_segments=merged_segments+list(audio[start:end])
    stereo_audio = np.array((merged_segments, merged_segments)).T 
    return np.mean(stereo_audio, axis=-1)     

def break_file_to_segments(path,file,segments_list,break_length,snr,thresh=None,compress=False,remove_silence=False,target_fs=None):
    path_to_audio=os.path.join(path,file)
    audio=np.zeros(5)
    if compress:
        path_to_audio,audio,fs=compress_range(path_to_audio,thresh) 
    else:   
        audio,fs=read_audio(path_to_audio,target_fs,mono=True)
        
    audio=butter_lowpass_filter(audio,1700, fs, order=5)
    if remove_silence:
        speech_segs = aS.silence_removal(audio, fs, 0.020, 0.020, smooth_window = 0.5, weight = 0.3, plot = False)
        audio=clean_silence(audio,speech_segs,fs)       
    audio = normalize_to_energy(audio, db=snr)
    audio_len=len(audio)/fs
    one_and_h_list=[break_length]*len(segments_list)
    total_list_with_breaks=[*segments_list, *one_and_h_list]
    total_segment=sum(total_list_with_breaks)
    rep_segments=[0]
    if audio_len<total_segment:
        total_segment=1
        rep_segments=[audio_len]
    else:        
        num_rep= audio_len// total_segment
        rep_segments=segments_list*int(num_rep)
    segments_df=pd.DataFrame(columns=["seg_start","seg_end","seg_duration","segments"]) 
    segments_df["seg_duration"]=rep_segments
    segments_df["seg_end"]=segments_df["seg_duration"].cumsum()
    segments_df["seg_start"]=0
    a=[0]+list(segments_df["seg_duration"].cumsum())
    segments_df["seg_start"]=a[:-1]     
    segments_df["onsets"]=fs*segments_df["seg_start"]
    segments_df["onsets"]=segments_df["onsets"].astype(int) 
    segments_df["offsets"]=fs*segments_df["seg_end"]
    segments_df["offsets"]=segments_df["offsets"].astype(int)
    segments=[]
    onsets=segments_df["onsets"].values   #
    offsets=segments_df["offsets"].values #
    del segments_df["offsets"]
    del segments_df["onsets"]
    for s,e in zip(onsets,offsets):
        segments.append(audio[s:e])
    segments_df["segments"]=segments
    
    segments_df=segments_df[segments_df["seg_duration"]!=break_length]
    segments_df=shuffle(segments_df[["seg_duration","segments"]])
    return segments_df.reset_index(drop=True),fs

def apply_sequence(segment):
    segment_1=[0]
    applied_effects=0
    if 1:#random.randint(0, 1)
        segment_1=segment
    #apply effects
        if random.randint(0, 1):
            apply_reverb,apply_phaser,apply_lowpass=0,0,0
            cut_f=[300,350,400][random.randint(0,2)]        
            applied_effects=apply_reverb|apply_phaser|apply_lowpass       
    return  segment_1,applied_effects  

def fill_dict(duration,event_name,file_name,applied_effects):
    info_dict_1={}
    info_dict_1["duration"]=duration
    info_dict_1["event_name"]=event_name
    info_dict_1["file_name"]=file_name
    info_dict_1["applied_effects"]=applied_effects
    return info_dict_1

def organize_segment_df(boolean,dur,ev_name,file_name, app_eff,seg,sound_segment_df):
    df_1=pd.DataFrame()
    boolean=[1]
    if  any(boolean):
        info_dict=fill_dict(dur,ev_name,file_name,app_eff)   
        df_1=pd.DataFrame(info_dict, index=['i',])
        df_1["segment"] = 0
        df_1["segment"] = df_1["segment"].astype('object')
        df_1.at['i',"segment"]=seg        
        sound_segment_df=pd.concat([sound_segment_df,df_1]) 
    return sound_segment_df.reset_index(drop=True) 

def regulate_duration(df,tg_duration,s_duration):
    df["summed_duration"]=df["duration"].cumsum()
    df["tg_relative"]=False
    df.loc[df[df["summed_duration"]<tg_duration].index,"tg_relative"]=True
    df.loc[df[df["summed_duration"]>s_duration].index,"tg_relative"]=True
    df=df[df["tg_relative"]==True]
    del df["tg_relative"]
    del df["summed_duration"]
    return df

def create_segment_doc(df):  
    df["new_end"]=df["duration"].cumsum()
    df["new_start"]=0
    a=[0]+list(df["duration"].cumsum())
    df["new_start"]=a[:-1]
    df=df[["new_start","new_end","event_name"]]
    return df

def write_to_file(sound_segment_df,path_to_info,path_to_seg,remove_noise=False):    
    temp_df=sound_segment_df[["duration","event_name","file_name","applied_effects"]]
    if remove_noise:
        temp_df=temp_df[temp_df["event_name"]!="noise"]
    temp_df.to_csv(path_to_info,sep=" ", index= False,header=False)
    temp_df=temp_df[["duration","event_name"]]
    csv_file=create_segment_doc(temp_df)
    csv_file.to_csv(path_to_seg,sep=" ", index= False,header=False)
    
    
def create_audio(sound_segment_df,remove_noise):
    stereo_audio=None
    if remove_noise:
        sound_segment_df=sound_segment_df[sound_segment_df["event_name"]!="noise"]
    if sound_segment_df.shape[0]!=0:    
        sound_segment_df["acc_sum"]=sound_segment_df["duration"].cumsum()
        event_audio=list(itertools.chain(*sound_segment_df["segment"].values))
        random_state = np.random.RandomState(1234)
        scene_audio = random_state.uniform(0., 1., len(event_audio))
        scene_audio = normalize_to_energy(scene_audio, db=0)    
        stereo_audio = np.array((event_audio, scene_audio)).T
        stereo_audio /= np.max(np.abs(stereo_audio)) 
    return stereo_audio  

def create_records(sound_segment_df,audio_file,file_num,sound_dir,info_dir,seg_dir,output_dir,fs,remove_noise=False):
    #Write audio with noise

    stereo_audio=create_audio(sound_segment_df,remove_noise=remove_noise) 
 #   print("str",stereo_audio)
    if any(stereo_audio[0]):
        path_to_audio=os.path.join(output_dir,sound_dir,audio_file) 
        write_audio(path_to_audio, stereo_audio, fs)
        path_to_info=os.path.join(output_dir,info_dir,str(file_num))
        path_to_seg=os.path.join(output_dir,seg_dir,str(file_num))
        write_to_file(sound_segment_df,path_to_info,path_to_seg,remove_noise)  
    

def mix_file(path,path_noise,out_dir,file_num):
    output_dir=out_dir
    #get_sound_files
    path_1,file_1,path_2,file_2=get_two_files_randomly(path) 
    #get_noise_file
    sound_segment_df=pd.DataFrame(columns=["duration","event_name","file_name","applied_effects","segment"])
    _,file_noise=get_file_randomly(path_noise) 
    file_1_segments,fs_1=break_file_to_segments(path_1,file_1,[2,3],0.2,20,-20,compress=True,remove_silence=True,target_fs=None)
    file_2_segments,fs_2=break_file_to_segments(path_2,file_2,[2,3],0.2,20,-40,compress=True,remove_silence=True,target_fs=None)
 #   print(file_1,file_2)
    min_len_df=min(file_1_segments.shape[0],file_2_segments.shape[0])    
    noise_segments,fs_noise=break_file_to_segments(path_noise,file_noise,[0.2,0.3],1,10,None,compress=False,remove_silence=False,target_fs=fs_1) 
    
    for i,(seg_1,seg_2) in enumerate(zip(file_1_segments["segments"][:min_len_df],file_2_segments["segments"][:min_len_df])):
        #Get first segment
        segment_1,app_effects_1=apply_sequence(seg_1)
     #   print("seg1",segment_1)
        duration_1=file_1_segments["seg_duration"][i]
     #   print(duration_1)
        #Get second segment
        segment_2,app_effects_2=apply_sequence(seg_2)
     #   print("seg2",segment_2)
      #  segment_2,app_effects_2=seg_2,False
        duration_2=file_2_segments["seg_duration"][i]
      #  print(duration_2)
        #Get noise segment
        apply_noise=[random.randint(0, 1)]
        noise_duration=0
        noise_segment=0
        if apply_noise:
           noise_ind=random.randint(0, len(noise_segments)-1)
           noise_segment=noise_segments["segments"][noise_ind]
           noise_duration=noise_segments["seg_duration"][noise_ind]
        apply_overlay=0
        mixed_seg=[0]
        if (duration_1==1.5)&(duration_2==1.5):
            apply_overlay=random.randint(0, 1)
            if apply_overlay:
                mixed_seg,fs=overlay_sounds(seg_1,seg_2,fs_1,fs_2)
        mixed_seg_duration=0        
        if any(mixed_seg):  
           mixed_seg_duration=len(mixed_seg)/fs_1
           tot_file_name=file_1+"_"+file_2
           sound_segment_df=organize_segment_df(mixed_seg,mixed_seg_duration,"mixed_sounds",tot_file_name,2,mixed_seg,sound_segment_df)     
           sound_segment_df=organize_segment_df(apply_noise,noise_duration,"noise",file_noise,0,noise_segment,sound_segment_df)  
        else:
           sound_segment_df=organize_segment_df(segment_1,duration_1,"speakerA",file_1,app_effects_1,segment_1,sound_segment_df)    
           sound_segment_df=organize_segment_df(apply_noise,noise_duration,"noise",file_noise,0,noise_segment,sound_segment_df)  
           sound_segment_df=organize_segment_df(segment_2,duration_2,"speakerB",file_2,app_effects_2,segment_2,sound_segment_df)
        
    sound_segment_df=regulate_duration(sound_segment_df,60,0.5)
    audio_file=str(file_num)+".wav"
    if sound_segment_df.shape[0]!=0:
        create_records(sound_segment_df,audio_file,file_num,"sounds","sounds_info","sounds_segments",output_dir,fs_1,remove_noise=False) 

        create_records(sound_segment_df,audio_file,file_num,"sounds_no_noise",
                       "sounds_no_noise_info","sounds_no_noise_segments",output_dir,fs_1,remove_noise=True) 
    
def create_data_set(args):
    for j in range(1166,args.num_of_files):
        start=time.time()
        mix_file(args.path,args.path_noise,args.output_dir,j)    
    
    
if __name__ == '__main__':        
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--output_dir',default=' ', type=str) 
    parser.add_argument('--path',default='path/to/files', type=str) 
    parser.add_argument('--path_noise', default='path/to/background_noise',type=str) 
    parser.add_argument('--num_of_files',default=5000, type=int)
    args = parser.parse_args()
    
    create_data_set(args)